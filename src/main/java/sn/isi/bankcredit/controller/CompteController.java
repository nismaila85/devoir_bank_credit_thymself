package sn.isi.bankcredit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/compte")
public class CompteController {

    @GetMapping("/list")
    public String list() {
        return "compte/list";
    }

    @GetMapping("/add")
    public String add() {
        return "compte/add";
    }
}
